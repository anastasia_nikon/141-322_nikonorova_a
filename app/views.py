from django.shortcuts import render, redirect, render_to_response
from .forms import PartnersForm, PartnersFormNew
from .models import News, Contacts, Partners, Speakers, Presentations, Organisators
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.template import RequestContext
from django.views.decorators.cache import cache_page
@cache_page(60 * 1)
def index(request):
    pres_list = Presentations.objects.all()
    context = {'presentations': pres_list}
    return render(request, 'presentations/base_pres.html', context)

def contacts(request):
    contacts_list = Contacts.objects.all()
    context = {'contacts': contacts_list}
    return render(request, 'contacts/base_contacts.html', context)

def partners(request):
    part_list = Partners.objects.all()
    context = {'partners': part_list}
    return render(request, 'partners/base_partners.html', context)

def partners_filter(request):
    part_list = Partners.objects.all()
    context = {'partners': part_list}
    return render(request, 'partners/base_partners_filter.html', context)

def speakers(request):
    speakers_list = Speakers.objects.all()
    context = {'speakers': speakers_list}
    return render(request, 'speakers/base_speakers.html', context)

def presentations(request):
    pres_list = Presentations.objects.all()
    context = {'presentations': pres_list}
    return render(request, 'presentations/base_pres.html', context)

def organisators(request):
    org_list = Organisators.objects.all()
    context = {'organisators': org_list}
    return render(request, 'organisators/base_org.html', context)

def news(request):
     news_list = News.objects.all()
     paginator = Paginator(news_list, 2)
     page = request.GET.get('page')
     try:
         news = paginator.page(page)
     except PageNotAnInteger:
         news = paginator.page(1)
     except EmptyPage:
         news = paginator.page(paginator.num_pages)
     context = {'news': news}
     return render_to_response('news/list.html', context ,context_instance=RequestContext(request))

def partners_edit(request, p_id):
    partner = Partners.objects.get(id = p_id)
    if request.method=='POST' :
        form = PartnersForm(request.POST, instance=partner)
        if form.is_valid():
            form.save()
            return redirect('/app/partners/')
    else:
        form = PartnersForm(instance=partner)
    context = {'form': form}
    return render(request, 'partners/change.html', context)
    

def partners_new(request):
    if request.method=='POST' :
        form = PartnersFormNew(request.POST, request.FILES)
        if form.is_valid():
            form.save()
            return redirect('/app/partners/')
    else:
        form = PartnersFormNew()
    context = {'form': form}
    return render(request, 'partners/change.html', context)
    
    
def partners_del(request, p_id):
    partner_del = Partners.objects.get(id = p_id)
    partner_del.delete()
    return redirect('/app/partners/')