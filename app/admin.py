from django.contrib import admin

# Register your models here.
from .models import News, Contacts, Partners, Speakers, Presentations, Organisators

class news_set(admin.ModelAdmin):
    list_display = ('title', 'description', 'pub_date')
    search_fields = ['title']

class contacts_set(admin.ModelAdmin):
    list_display = ('name', 'phone', 'email')
    search_fields = ['name']

class partners_set(admin.ModelAdmin):
    list_display = ('name', 'description', 'bool')
    search_fields = ['name']

class speakers_set(admin.ModelAdmin):
    list_display = ('name', 'description', 'pres_date')
    search_fields = ['name']

class presentations_set(admin.ModelAdmin):
    list_display = ('title', 'description', 'pres_date')
    search_fields = ['title']

class organisators_set(admin.ModelAdmin):
    list_display = ('name', 'phone', 'email')
    search_fields = ['name']


admin.site.register(News, news_set)
admin.site.register(Contacts, contacts_set)
admin.site.register(Partners, partners_set)
admin.site.register(Speakers, speakers_set)
admin.site.register(Presentations, presentations_set)
admin.site.register(Organisators, organisators_set)
