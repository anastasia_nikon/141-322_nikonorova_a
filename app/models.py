from django.db import models

class News(models.Model):
 title = models.CharField(max_length=70)
 description = models.CharField(max_length=255)
 pub_date = models.DateTimeField()
 text = models.TextField()


class Contacts(models.Model):
 name = models.CharField(max_length=255)
 description = models.CharField(max_length=255)
 phone = models.CharField(max_length=70)
 email = models.CharField(max_length=70)


class Partners(models.Model):
 name = models.CharField(max_length=255)
 description = models.CharField(max_length=255)
 image = models.ImageField(upload_to='images_up')
 bool = models.BooleanField("Publicate", default=True)


class Speakers(models.Model):
 name = models.CharField(max_length=255)
 description = models.CharField(max_length=255)
 pres_date = models.DateTimeField()
 theme = models.CharField(max_length=255)


class Presentations(models.Model):
 title = models.CharField(max_length=255)
 description = models.CharField(max_length=255)
 pres_date = models.DateTimeField()


class Organisators(models.Model):
 name = models.CharField(max_length=255)
 description = models.CharField(max_length=255)
 phone = models.CharField(max_length=70)
 email = models.CharField(max_length=70)