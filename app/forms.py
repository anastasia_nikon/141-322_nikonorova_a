# coding: utf-8

from django.forms import ModelForm
from . models import Partners

class PartnersForm(ModelForm):
    class Meta:
        model = Partners
        fields = ['name', 'description', 'bool']
        
class PartnersFormNew(ModelForm):
    class Meta:
        model = Partners
        fields = ['name', 'image', 'description', 'bool']
