from django.conf.urls import patterns, url
from app import views
urlpatterns = patterns('',
 url(r'^$', views.index, name='index'),
 url(r'^news/$', views.news, name='news'),
 url(r'^organisators/$', views.organisators, name='organisators'),
 url(r'^contacts/$', views.contacts, name='contacts'),
 url(r'^presentations/$', views.presentations, name='presentations'),
 url(r'^speakers/$', views.speakers, name='speakers'),
 url(r'^partners/$', views.partners, name='partners'),
 url(r'^partners/filter$', views.partners_filter, name='partners_filter'),
 url(r'^partners/add/$', views.partners_new, name='partners_new'),
 url(r'^partners/del/(?P<p_id>\d+)/$', views.partners_del, name='partners_del'),
 url(r'^partners/change/(?P<p_id>\d+)/$', views.partners_edit, name='partners_edit'),)