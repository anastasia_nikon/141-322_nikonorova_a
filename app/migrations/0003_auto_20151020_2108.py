# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0002_contacts_organisators_partners_presentations_speakers'),
    ]

    operations = [
        migrations.AlterField(
            model_name='partners',
            name='image',
            field=models.ImageField(upload_to=b'images_up'),
        ),
    ]
